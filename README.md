# Middleware de Controle para Ambientes Experimentais Sem Fio
Middleware de Controle para Ambientes Experimentais Sem Fio criado pelo GERCOM - www.gercom.ufpa.br

Author: **Alexsander Núñez** alexsander.nunez@gmail.com

Foi utilizado o Start Bootstrap para criação deste código.
Start Bootstrap foi criado por e é mantido por **[David Miller](http://davidmiller.io/)**, Proprietário de [Blackrock Digital](http://blackrockdigital.io/) e baseia-se no framework [Bootstrap](http://getbootstrap.com/) criado por [Mark Otto](https://twitter.com/mdo) e [Jacob Thorton](https://twitter.com/fat).

https://twitter.com/davidmillerskt

https://github.com/davidtmiller

###### Os arquivos baixados deve estar localizados na mesma pasta do servidor web (tomcat, apache,...). Exemplo: /var/www/html/midd/

1 - Instale o php, python, curl e ssh: **apt-get install php5 python curl ssh**

2 - Instale as dependencias para execução do Middleware: **sudo python depend_install.py***

3 - Inicialize o middleware no servidor: **sudo python COstart.py --ip [ip-servidor]**

4 - Acesse via web: **http://[ip-servidor]/midd/**

###### Segue abaixo comandos que podem ser executador via terminal:
1 - Ligar nodo experimental: curl -X POST -d '{"jsonrpc": "2.0","method": "controle","params": [{"status":"on"}],"id": "1"}' http://[ip-nodo-exp]:8080/api ou wget [ip-nodo-exp]/on assim como qualquer outro método que utilize http

2 - Desligar nodo experimental: curl -X POST -d '{"jsonrpc": "2.0","method": "controle","params": [{"status":"off"}],"id": "1"}' http://[ip-nodo-exp]:8080/api ou wget [ip-nodo-exp]/off assim como qualquer outro método que utilize http

3 - Status do nodo experimental: curl -X POST -d '{"jsonrpc": "2.0","method": "controle","params": [{"status":"status"}],"id": "1"}' http://[ip-nodo-exp]:8080/api ou wget [ip-nodo-exp]/status assim como qualquer outro método que utilize http

Seja Feliz !!!


