<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>Admin - Controle do Ambiente Experimental</title>

    <!-- Bootstrap -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Bootstrap Core CSS -->
    <link href="../vendor/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- MetisMenu CSS -->
    <link href="../vendor/metisMenu/metisMenu.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="../dist/css/sb-admin-2.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="../vendor/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">

    <!-- Morris Charts CSS -->
    <link href="../vendor/morrisjs/morris.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>
  <body>

<div class="row">
	<div class="col-lg-12">
		<div class="panel panel-default">
                	<div class="panel-heading"> Administrador, Seja Bem Vindo. Seu número de IP é <font color="red"><script type="text/javascript" src="ip.php"></script></font>
 		</div>
 
	<div class="row">
			<div class="col-lg-6">
					<div class="panel panel-default">
                        <div class="panel-heading">
                            Informações Gerais do Servidor <font color="red">omf
</font>

                        </div>
						<div class="panel-body">
                                			        Info's Memoria:
                                                                <pre><?php $memoria = shell_exec('free -m'); echo "$memoria"; ?></pre>
                                                                Info's Disco:
                                                                <pre><?php $disco = shell_exec('df -h'); echo "$disco"; ?></pre>
        							Usuários Logados:
        							<pre><?php $logado = shell_exec('w'); echo "$logado"; ?></pre>
							        Informação gerada em <?php $data = shell_exec('date'); echo "$data"; ?>

	                        		</div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="panel panel-default">
                        <div class="panel-heading"> Tráfego de Rede do Servidor </div>

                        <div class="panel-body">
                            <div class="flot-chart">
                                <div class="flot-chart-content" id="flot-line-chart-moving"></div>
                            </div>
                        </div>
                    </div>
                </div>

	</div> <!-- fechando row info | trafego -->
	<div class="row">
			<div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            Nodo's de Controle
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#gw" data-toggle="tab">Nodo Gateway</a>
                                </li>
                                <li><a href="#13" data-toggle="tab">Nodo 13</a>
                                </li>
                                <li><a href="#20" data-toggle="tab">Nodo 20</a>
                                </li>
                                <li><a href="#24" data-toggle="tab">Nodo 24</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="gw">
                                    <p> Hostname: <font color="red">10Gateway</font></p>
				    <p>	IP wan: <font color="red">10.126.1.222</font></p>
				    <p>	IP lan: <font color="red">10.130.11.10</font></p>
				    <p>         <font color="red">172.16.3.10</font></p>

        	                	<p><a href="http://172.16.3.10" target="_blank">Gerência Lucy</a> </p>
                                </div>
                                <div class="tab-pane fade" id="13">
                                    <p> Hostname: <font color="red">13Nodo</font></p>
                                    <p> IP wan: <font color="red">0.0.0.0</font></p>
                                    <p> IP lan: <font color="red">0.0.0.0</font></p>
				    <p>         <font color="red">0.0.0.0</font></p>

					<p><a href="http://172.16.3.13" target="_blank">Gerência Lucy</a> </p>
				</div>
                                <div class="tab-pane fade" id="20">
                                    <p> Hostname: <font color="red">20Nodo</font></p>
                                    <p> IP wan: <font color="red">10.126.1.246</font></p>
                                    <p> IP lan: <font color="red">10.130.11.80</font></p>
				    <p>         <font color="red">172.16.3.80</font></p>

					<p><a href="http://172.16.3.80" target="_blank">Gerência Lucy</a> </p>
                                </div>
                                <div class="tab-pane fade" id="24">
                                    <p> Hostname: <font color="red">24Nodo</font></p>
                                    <p> IP wan: <font color="red">10.126.1.210</font></p>
                                    <p> IP lan: <font color="red">10.130.11.90</font></p>
				    <p>         <font color="red">172.16.3.90</font></p>

					<p><a href="http://172.16.3.90" target="_blank">Gerência Lucy</a> </p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

		<div class="col-lg-4">
                    <div class="panel panel-default">
                        <div class="panel-heading">
                            <form method="POST" action="habilitamidd.php">Nodo's de Experimentação<input align="right" type="submit" value="Habilitar middleware"></form>
                        </div>
                        <!-- /.panel-heading -->
                        <div class="panel-body">
                            <ul class="nav nav-tabs">
                                <li class="active"><a href="#e13" data-toggle="tab">Icarus 13</a>
                                </li>
                                <li><a href="#e20" data-toggle="tab">Icarus 20</a>
                                </li>
                                <li><a href="#e24" data-toggle="tab">Icarus 24</a>
                                </li>
                            </ul>

                            <!-- Tab panes -->
                            <div class="tab-content">
                                <div class="tab-pane fade in active" id="e13">
                                    <p> Informaçoes
                                    <form method="POST" action="arquivo13.php" ><input type="submit" name="info13" value="Status 13"></button></form>
									<form method="POST" action="ligar13.php" ><input type="submit" name="ligar13" value="Ligar 13"></button></form>
									<form method="POST" action="desligar13.php" ><input type="submit" name="desligar13" value="Desligar 13"></button></form>
                                </div>
                                <div class="tab-pane fade" id="e20">
                                    <p> Informações
                                    <form method="POST" action="arquivo20.php" ><input type="submit" name="info20" value="Status 20"></button></form>
									<form method="POST" action="ligar20.php"><input type="submit" name="ligar20" value="Ligar 20"></button></form>
									<form method="POST" action="desligar20.php"><input type="submit" name="desligar20" value="Desligar 20"></button></form>
				</div>
                                <div class="tab-pane fade" id="e24">
                                    <p>	Informações
                                    <form method="POST" action="arquivo24.php"><input type="submit" name="info24" value="Status 24"></button></form>
									<form method="POST" action="ligar24.php" ><input type="submit" name="ligar24" value="Ligar 24"></button></form>
									<form method="POST" action="desligar24.php" ><input type="submit" name="desligar24" value="Desligar 24"></button></form>
				</div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-3">
				<div class="panel panel-default">
					<div class="panel-heading">
						<i class="fa fa-bell fa-fw"></i> Painel de Notificação
                     </div>
                     <div class="panel-body">
						<div class="list-group">
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-envelope fa-fw"></i> Message Receive
                                    <span class="pull-right text-muted small"><em>40 minutes ago</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-tasks fa-fw"></i> Middleware
                                    <span class="pull-right text-muted small"><em>8 minutes ago</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-upload fa-fw"></i> Server Rebooted
                                    <span class="pull-right text-muted small"><em>16:20 PM</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-bolt fa-fw"></i> Server Crashed!
                                    <span class="pull-right text-muted small"><em>10:57 AM</em>
                                    </span>
                                </a>
                                <a href="#" class="list-group-item">
                                    <i class="fa fa-warning fa-fw"></i> Server Not Responding
                                    <span class="pull-right text-muted small"><em>13:13 PM</em>
                                    </span>
                                </a>

                        </div>
                        <a href="#" class="btn btn-default btn-block">Visualizar Todas</a>
					</div>
				</div>
                </div>
		</div> <!-- fechando: row controle | experimental  | painel-->
	    
	       
	    </div> <!-- fechando: panel panel-default geral -->
	
	</div> <!-- fechando: col-lg-12 geral -->
</div> <!-- fechando: row geral -->           
                    
    <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <!-- Include all compiled plugins (below), or include individual files as needed -->
    <script src="js/bootstrap.min.js"></script>
    <!-- /#wrapper -->
    <!-- jQuery -->
    <script src="../vendor/jquery/jquery.min.js"></script>
    <!-- Bootstrap Core JavaScript -->
    <script src="../vendor/bootstrap/js/bootstrap.min.js"></script>
    <!-- Metis Menu Plugin JavaScript -->
    <script src="../vendor/metisMenu/metisMenu.min.js"></script>
    <!-- Flot Charts JavaScript -->
    <script src="../vendor/flot/excanvas.min.js"></script>
    <script src="../vendor/flot/jquery.flot.js"></script>
    <script src="../vendor/flot/jquery.flot.pie.js"></script>
    <script src="../vendor/flot/jquery.flot.resize.js"></script>
    <script src="../vendor/flot/jquery.flot.time.js"></script>
    <script src="../vendor/flot-tooltip/jquery.flot.tooltip.min.js"></script>
    <script src="../data/flot-data.js"></script>
    <!-- Custom Theme JavaScript -->
    <script src="../dist/js/sb-admin-2.js"></script>
    
    
  </body>
</html>


