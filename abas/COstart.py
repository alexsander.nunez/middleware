#!/usr/bin/env python
# -*- coding: utf-8 -*-
import os
import sys

from flask import Flask, render_template
import argparse
PROJECT_DIR, PROJECT_MODULE_NAME = os.path.split(
    os.path.dirname(os.path.realpath(__file__))
)

FLASK_JSONRPC_PROJECT_DIR = os.path.join(PROJECT_DIR, os.pardir)
if os.path.exists(FLASK_JSONRPC_PROJECT_DIR) \
        and not FLASK_JSONRPC_PROJECT_DIR in sys.path:
    sys.path.append(FLASK_JSONRPC_PROJECT_DIR)

from flask_jsonrpc import JSONRPC

app = Flask(__name__)
app.config.from_object(__name__)
jsonrpc = JSONRPC(app, '/api')

@app.route('/')
def web():
    return render_template('index.html')

import lib.COserver

if __name__ == '__main__':
    parser = argparse.ArgumentParser(prog='Servidor ESP')
    parser.add_argument('--ip', help='Informe o ip da sua maquina')
    parser.add_argument('--ifconfig', help='Comando para visualizar o ip da maquina')
    args = parser.parse_args()

    port = int(os.environ.get('PORT', 8080))
    app.run(host=args.ip, port=port, debug=True)
